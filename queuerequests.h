#ifndef QUEUEREQUESTS_H
#define QUEUEREQUESTS_H

#include <QMutex>
#include <QWaitCondition>
#include <stack>
#include "expression.h"

class TaskThread;

/** \brief Очередь запросов на вычисление выражений
  */
class QueueRequests : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int size READ size NOTIFY sizeChanged)
public:

    QueueRequests();
    ~QueueRequests();

    void exitAll();

    /// \brief Поместить запрос на вычисление в очередь
    void push(const Expression &item);

    /// \brief Установить время вычисления выражения в секундах
    void setWaitTime_sec(unsigned long val);

    /// \brief Размер очереди
    int size();

protected:

    void run();

private:

    bool m_wantExit;                    ///< \brief флаг необходимости завершить работу
    QMutex m_mutex;                     ///< \brief мьютекс, разграничивающий доступ внутренни параметрам
    QWaitCondition m_availableItem;
    unsigned long m_waitTime_sec;       ///< \brief время вычисления выражения в секундах
    std::stack<Expression> m_stack;     ///< \brief Очередь выражений

    bool dequeue__(Expression &item, unsigned long &time_sec);

    friend class TaskThread;

signals:

    void sizeChanged();
};

#endif // QUEUEREQUESTS_H
