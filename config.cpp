#include "config.h"
#include <QDir>
#include <QCoreApplication>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(config, "config", QtWarningMsg)

Config::Config()
{
    m_width = 500;
    m_height = 500;
    m_x = 10;
    m_y = 10;

    QDir dir;
    dir.setPath(configPath());
    if(!dir.exists()) {
        if(!dir.mkdir(configPath())) {
            qCWarning(config) << QString("Couldn't create config path: %1").arg(configPath());
        }
    }
    if(dir.exists()) {
        m_configFile = dir.path() + QDir::separator() + QString::fromUtf8("settings.cfg");
    }
    else {
        m_configFile = QString::fromUtf8("settings.cfg");
    }

    if(!QFileInfo::exists(m_configFile)) {
        write();
    }
    else {
        read();
    }
}

QString Config::configPath() const {

    QStringList lst = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    if(!lst.empty()) {
        return lst.last() + QDir::separator() + QString::fromUtf8(".calculator");
    }

    return QCoreApplication::applicationDirPath();
}

Config &Config::instance() {
    static Config *inst__ = new Config();
    return *inst__;
}

bool Config::parseArg(const QString &prm, const QString &line, QVariant &value) const {

    QStringList lst = line.split(" ", QString::SkipEmptyParts);
    if(2 != lst.size()) {
        return false;
    }

    if(prm != lst.at(0)) {
        return false;
    }

    value.setValue(lst.at(1));
    return true;
}

bool Config::write() {

    QFile file(m_configFile);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream out(&file);

    out << QString("width %1\r\n").arg(m_width);
    out << QString("height %1\r\n").arg(m_height);
    out << QString("x %1\r\n").arg(m_x);
    out << QString("y %1\r\n").arg(m_y);

    return true;
}

bool Config::read() {

    QFile file(m_configFile);

    if(!file.exists()) {
        return false;
    }

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream in(&file);
    QString line;
    QVariant value;

    while(in.readLineInto(&line)) {
        line = line.trimmed();

        if(line.isEmpty()) {
            continue;
        }

        if(line.at(0) == '#') {
            continue;
        }

        if(parseArg("width", line, value) && value.canConvert(QMetaType::Int)) {
            m_width = value.toInt();
        }
        else if(parseArg("height", line, value) && value.canConvert(QMetaType::Int)) {
            m_height = value.toInt();
        }
        else if(parseArg("x", line, value) && value.canConvert(QMetaType::Int)) {
            m_x = value.toInt();
        }
        else if(parseArg("y", line, value) && value.canConvert(QMetaType::Int)) {
            m_y = value.toInt();
        }

    }

    return true;
}

void Config::setWidth(int val) {

    if(m_width != val) {
        m_width = val;
        emit widthChanged();
    }
}

void Config::setHeight(int val) {

    if(m_height != val) {
        m_height = val;
        emit heightChanged();
    }
}

void Config::setX(int val) {

    if(m_x != val) {
        m_x = val;
        emit xChanged();
    }
}

void Config::setY(int val) {

    if(m_y != val) {
        m_y = val;
        emit yChanged();
    }
}
