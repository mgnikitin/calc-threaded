#ifndef CALC_H
#define CALC_H

namespace calc {

    /// \brief Тип операции
    enum OperatorType {
        Undefined = -1,     ///< \brief не опеределено
        Addition,           ///< \brief сложение
        Substraction,       ///< \brief вычитание
        Multiplication,     ///< \brief умножение
        Division            ///< \brief деление
    };

    /// \brief Код ошибки
    enum ErrorType {
        None,       ///< \brief нет ошибки
        Divide,     ///< \brief ошибка деления на 0
        Incorrect   ///< \brief выражение некорректно
    };

    /** \brief Функция выполения вычислений
      * \param TypeWork - тип операции
      * \param OperandA - операнд A
      * \param OperandB - операнд B
      * \param ErrorCode - код ошибки
      * \return результат вычислений
      */
    double DoIt(OperatorType TypeWork, double OperandA, double OperandB, ErrorType& ErrorCode);

}

#endif // CALC_H
