#include "taskthread.h"
#include "queueresults.h"
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(taskThread, "taskThread", QtWarningMsg)

TaskThread::TaskThread(QueueRequests *queue) {
    m_queue = queue;
}

TaskThread::~TaskThread() {

    wait();
}

void TaskThread::run() {

    Expression item;
    unsigned long time_sec;

    do {
        // dequeue
        if(!m_queue->dequeue__(item, time_sec)) {
            break;
        }

        doTask(item, time_sec);


    } while(true);
}


void TaskThread::doTask(const Expression &args, unsigned long time_sec) {

    qCDebug(taskThread) << QString("doTask: %1").arg(args.textLine());

    calc::ErrorType errCode;
    const double val = args.evaluate(errCode);
    sleep(time_sec);
    QueueResults::instance().enqueue(args, val, errCode);
}
