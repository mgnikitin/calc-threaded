#ifndef KEYPROCESSOR_H
#define KEYPROCESSOR_H

#include <QObject>
#include <QString>
#include "expression.h"
#include "queuerequests.h"
#include "taskthread.h"

/** \brief Обработчик символов ввода
  */
class KeyProcessor : public QObject {
    Q_OBJECT
    Q_DISABLE_COPY(KeyProcessor)

    Q_PROPERTY(QString textLine READ textLine NOTIFY textLineChanged)
    Q_PROPERTY(int time READ time WRITE setTime NOTIFY timeChanged)

public:

    ~KeyProcessor();

    static KeyProcessor &instance();

    /// \brief Добавить символ
    Q_INVOKABLE void append(QString str);

    /// \brief выражение в виде текстовой записи
    QString textLine() const;

    /// \brief Вернуть время вычисления выражения в секундах
    int time();

    /// \brief Установить время вычисления выражения в секундах
    void setTime(int time);

    QueueRequests &queue();

private:

    Expression m_expression;    ///< \brief выражение

    QueueRequests m_queue;      ///< \brief очередь запросов на вычисление
    TaskThread *m_thread;
    int m_timeSec;              ///< \brief время вычисления выражения в секундах

    KeyProcessor();

    void calculate();

signals:

    void textLineChanged();
    void timeChanged();

};

#endif // KEYPROCESSOR_H
