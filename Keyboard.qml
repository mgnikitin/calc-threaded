import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property int keySize: 70

    width: keySize * 3 + 20
    height: keySize * 4 + 20

    ListModel {
        id: keyModel

        ListElement { key: "7" }
        ListElement { key: "8" }
        ListElement { key: "9" }
        ListElement { key: "4" }
        ListElement { key: "5" }
        ListElement { key: "6" }
        ListElement { key: "1" }
        ListElement { key: "2" }
        ListElement { key: "3" }
        ListElement { key: "0" }
        ListElement { key: "." }

    }

    GridView {
        anchors.centerIn: parent

        width: keySize * 3 + 20
        height: parent.height - 10
        cellWidth: keySize + 5
        cellHeight: keySize + 5

        interactive: false

        model: keyModel

        delegate: Button {
            width: keySize
            height: keySize
            text: key

            onClicked: {
                KeyProcessor.append(text)
            }
        }
    }
}
