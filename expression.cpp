#include "expression.h"
#include <QDebug>

calc::OperatorType OperatorConvertor::fromChar(QChar chr, bool &ok) {

    ok = true;

    const char ch = chr.toLatin1();
    if('+' == ch) {
        return calc::Addition;
    }
    else if('-' == ch) {
        return calc::Substraction;
    }
    else if('*' == ch) {
        return calc::Multiplication;
    }
    else if('/' == ch) {
        return calc::Division;
    }

    ok = false;
    qWarning() << QString::fromUtf8("OperatorConvertor: undefined operator '%1'").arg(ch);
    return calc::Undefined;
}

QString OperatorConvertor::toString(const calc::OperatorType &t) {

    switch(t) {
    case calc::Addition: return "+";
    case calc::Substraction: return "-";
    case calc::Multiplication: return "*";
    case calc::Division: return "/";
    case calc::Undefined: return "";
    }

    // не должны сюда попасть
    return "";
}

bool Expression::append(QChar ch) {

    if(ch.isDigit() || (QChar('.') == ch)) {
        QString &operand = (calc::Undefined == m_operator) ? m_leftOperand : m_rightOperand;

        if(isValid(operand + ch)) {
            operand.append(ch);
            return true;
        }
    }
    else if(calc::Undefined == m_operator) {
        bool ok;
        const calc::OperatorType t = OperatorConvertor::fromChar(ch, ok);
        if(ok) {
            m_operator = t;
            return true;
        }
        qWarning() << QString::fromUtf8("Expression: undefined operator '%1'").arg(ch);
    }

    return false;
}

double Expression::evaluate(calc::ErrorType &errCode) const {

    bool ok;
    const double op1 = m_leftOperand.toDouble(&ok);
    if(!ok) {
        errCode = calc::Incorrect;
        return 0.0;
    }
    const double op2 = m_rightOperand.toDouble(&ok);
    if(!ok) {
        errCode = calc::Incorrect;
        return 0.0;
    }
    return calc::DoIt(m_operator, op1, op2, errCode);
}

QString Expression::textLine() const {

    return QString("%1 %2 %3").arg(m_leftOperand)
            .arg(OperatorConvertor::toString(m_operator)).arg(m_rightOperand);
}

int Expression::length() const {

    if(calc::Undefined != m_operator) {
        return m_leftOperand.size() + m_rightOperand.size() + 1;
    }
    else {
        return m_leftOperand.size() + m_rightOperand.size();
    }
}

bool Expression::isValid() const {

    return isValid(m_leftOperand) && isValid(m_rightOperand)
            && (calc::Undefined != m_operator);
}

bool Expression::isValid(QString str) const {

    bool ok;
    str.toDouble(&ok);
    return ok;
}
