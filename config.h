#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QString>
#include <QStandardPaths>

/// \brief Класс конфигурационного файла
class Config : public QObject {

    Q_OBJECT
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(int y READ y WRITE setY NOTIFY yChanged)

public:

    static Config &instance();

    QString configPath() const;

    Q_INVOKABLE bool write();
    bool read();

    int width() const { return m_width; }
    void setWidth(int val);

    int height() const { return m_height; }
    void setHeight(int val);

    int x() const { return m_x; }
    void setX(int val);

    int y() const { return m_y; }
    void setY(int val);

private:

    QString m_configFile;

    int m_width;
    int m_height;
    int m_x;
    int m_y;

    Config();

    Config(const Config &) = delete;
    Config &operator =(const Config &) = delete;

    bool parseArg(const QString &prm, const QString &line, QVariant &value) const;

signals:

    void widthChanged();
    void heightChanged();
    void xChanged();
    void yChanged();
};

#endif // CONFIG_H
