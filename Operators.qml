import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property int keySize: 70

    width: keySize * 3 + 20
    height: keySize * 3 + 20

    ListModel {
        id: operatorModel

        ListElement { name: "+" }
        ListElement { name: "-" }
        ListElement { name: "C" }
        ListElement { name: "*" }
        ListElement { name: "/" }
        ListElement { name: "=" }

    }

    GridView {
        anchors.centerIn: parent

        width: keySize * 3 + 20
        height: parent.height - 10
        cellWidth: keySize + 5
        cellHeight: keySize + 5

        interactive: false

        model: operatorModel

        delegate: Button {
            width: keySize
            height: keySize
            text: name

            onClicked: {
                KeyProcessor.append(text)
            }
        }
    }
}
