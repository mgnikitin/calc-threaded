#include "keyprocessor.h"
#include <QLoggingCategory>

KeyProcessor::KeyProcessor() {

    m_timeSec = 1;
    m_queue.setWaitTime_sec(m_timeSec);

    m_thread = new TaskThread(&m_queue);
    m_thread->start();
}

KeyProcessor::~KeyProcessor() {

    m_queue.exitAll();
    delete m_thread;
}

KeyProcessor &KeyProcessor::instance() {

    static KeyProcessor inst__;
    return inst__;
}

void KeyProcessor::append(QString str) {

    if(str.isEmpty()) { return; }

    QChar ch = str.at(0);

    if('=' == ch) {
        calculate();
    }
    else if('C' == ch) {
        m_expression.clear();
        emit textLineChanged();
    }
    else if((m_expression.length() < 20) && m_expression.append(ch)) {
        emit textLineChanged();
    }
}

void KeyProcessor::calculate() {

    if(0 != m_expression.length()) {
        m_queue.push(m_expression);
    }
    m_expression.clear();

    emit textLineChanged();
}

QString KeyProcessor::textLine() const {

    return m_expression.textLine();
}

int KeyProcessor::time() {

    return m_timeSec;
}

void KeyProcessor::setTime(int val) {

    if(m_timeSec != val) {
        if(val > 0) {
            m_timeSec = val;
            m_queue.setWaitTime_sec(m_timeSec);
        }
        emit timeChanged();
    }
}

QueueRequests &KeyProcessor::queue() {

    return m_queue;
}


