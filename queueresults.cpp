#include "queueresults.h"
#include <QMutexLocker>

//-----------------------------------------------------------------------------
QueueResults::QueueResults()
{

}

//-----------------------------------------------------------------------------
QueueResults::~QueueResults() {

    while(!m_data.empty()) {
        delete m_data.takeFirst();
    }
}

//-----------------------------------------------------------------------------
QQmlListProperty<ExpressionResult> QueueResults::results() {

    {
        QMutexLocker locker(&m_mutex);

        while(!m_queue.empty()) {
            Item item = m_queue.takeFirst();
            m_data.prepend(new ExpressionResult(item.args.textLine(), item.errCode, false));
            if(calc::None == item.errCode) {
                m_data.prepend(new ExpressionResult(QString("%1").arg(item.result, 0, 'f'), item.errCode, true));
            }
        }
    }


    return QQmlListProperty<ExpressionResult>(static_cast<QObject *>(this),
                                      static_cast<void *>(&m_data),
                                      &QueueResults::appendData,
                                      &QueueResults::countData,
                                      &QueueResults::atData,
                                      &QueueResults::clearData);
}

//-----------------------------------------------------------------------------
int QueueResults::size() {

    QMutexLocker locker(&m_mutex);

    return m_queue.size();
}

//-----------------------------------------------------------------------------
void QueueResults::enqueue(const Expression &args, const double &result, calc::ErrorType errCode) {

    {
        QMutexLocker locker(&m_mutex);

        m_queue.append({args, result, errCode});
    }

    emit notify();
}

//-----------------------------------------------------------------------------
QueueResults &QueueResults::instance() {

    static QueueResults inst__;
    return inst__;
}

//-----------------------------------------------------------------------------
void QueueResults::appendData(QQmlListProperty<ExpressionResult> *list, ExpressionResult *value) {

    QList<ExpressionResult *> *data = static_cast<QList<ExpressionResult *> *>(list->data);
    data->append(value);
}

//-----------------------------------------------------------------------------
int QueueResults::countData(QQmlListProperty<ExpressionResult> *list) {

    QList<ExpressionResult *> *data = static_cast<QList<ExpressionResult *> *>(list->data);
    return data->size();
}

//-----------------------------------------------------------------------------
ExpressionResult *QueueResults::atData(QQmlListProperty<ExpressionResult> *list, int index) {

    QList<ExpressionResult *> *data = static_cast<QList<ExpressionResult *> *>(list->data);
    return data->at(index);
}

//-----------------------------------------------------------------------------
void QueueResults::clearData(QQmlListProperty<ExpressionResult> *list) {

    QList<ExpressionResult *> *data = static_cast<QList<ExpressionResult *> *>(list->data);
    qDeleteAll(data->begin(), data->end());
    data->clear();
}
