#ifndef OPERATORARGS_H
#define OPERATORARGS_H

#include <QObject>
#include "calc.h"

class OperatorConvertor {
public:

    static calc::OperatorType fromChar(QChar, bool &ok);

    static QString toString(const calc::OperatorType &t);
};

/** \brief Класс математического выражения
  */
class Expression {
public:

    Expression() : m_operator(calc::OperatorType::Undefined) {}

    void clear() {
        m_leftOperand.clear();
        m_rightOperand.clear();
        m_operator = calc::OperatorType::Undefined;
    }

    /// \brief Добавить символ (цифра, оператор, ...)
    bool append(QChar ch);

    /** \brief Вычислить выражение
      * \param errCode - код ошибки, None в случае отсутствия ошибки
      * \return результат выражения
      */
    double evaluate(calc::ErrorType &errCode) const;

    /** \brief Представить выражение в виде текстовой записи
      */
    QString textLine() const;

    /** \brief Длина выражения с учетом знака
      */
    int length() const;

    /** \brief Проверка на валидность выражения
      */
    bool isValid() const;

private:

    QString m_leftOperand;          ///< \brief Левый операнд
    QString m_rightOperand;         ///< \brief Правый операнд
    calc::OperatorType m_operator;  ///< \brief Оператор выражения

    bool isValid(QString str) const;

};

#endif // OPERATORARGS_H
