#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include "queueresults.h"
#include "keyprocessor.h"
#include "config.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<ExpressionResult>("ExpressionResult", 1, 0, "ExpressionResult");

    QQmlApplicationEngine engine;
    QQmlContext* qmlRootContext = engine.rootContext();
    qmlRootContext->setContextProperty("QueueResults", &QueueResults::instance());
    qmlRootContext->setContextProperty("KeyProcessor", &KeyProcessor::instance());
    qmlRootContext->setContextProperty("Config", &Config::instance());
    qmlRootContext->setContextProperty("QueueRequests", &KeyProcessor::instance().queue());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
