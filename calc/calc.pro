#-------------------------------------------------
#
# Project created by QtCreator 2017-06-24T19:50:49
#
#-------------------------------------------------

QT       -= core gui

TARGET = calc
TEMPLATE = lib

INCLUDEPATH += ../include

SOURCES += calc.cpp

HEADERS += ../include/calc.h

DESTDIR = $$_PRO_FILE_PWD_/../lib
