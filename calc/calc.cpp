#include "calc.h"

double calc::DoIt(calc::OperatorType TypeWork, double OperandA, double OperandB, ErrorType &ErrorCode) {

    ErrorCode = calc::None;

    switch(TypeWork) {
    case calc::Addition: return OperandA + OperandB;
    case calc::Substraction: return OperandA - OperandB;
    case calc::Multiplication: return OperandA * OperandB;
    case calc::Division:
        if(0.0 == OperandB) {
            ErrorCode = calc::Divide;
            return 0.0;
        }
        return OperandA / OperandB;
    case calc::Undefined:
        ErrorCode = calc::Incorrect;
        return 0.0;
    }

    // не должны сюда попасть
    return 0.0;
}
