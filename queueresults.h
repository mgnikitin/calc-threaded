#ifndef QUEUERESULTS_H
#define QUEUERESULTS_H

#include <QObject>
#include <QMutex>
#include <QQmlListProperty>
#include <expression.h>

/// \brief Класс результата вычисления выражения
class ExpressionResult : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString text READ text CONSTANT)

    Q_PROPERTY(bool isError READ isError CONSTANT)

    Q_PROPERTY(bool isResult READ isResult CONSTANT)

public:

    ExpressionResult() : m_errCode(calc::None), m_result(false) {}

    ExpressionResult(const QString &text_, calc::ErrorType errCode, bool result)
        : m_text(text_), m_errCode(errCode), m_result(result) {}

    QString text() const { return m_text; }
    bool isError() const { return m_errCode != calc::None; }
    bool isResult() const { return m_result; }

private:
    QString m_text;
    calc::ErrorType m_errCode;
    bool m_result;
};

/** \brief Очередь результатов вычислений
  */
class QueueResults : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<ExpressionResult> results READ results NOTIFY notify)

public:

    ~QueueResults();

    QQmlListProperty<ExpressionResult> results();

    int size();

    /** \brief Поместить результат вычислений в очередь
      * \param args - выражение
      * \param result - результат
      * \param errCode - код ошибки
      */
    void enqueue(const Expression &args, const double &result, calc::ErrorType errCode);

    static QueueResults &instance();

private:

    /// \brief Элемент очереди
    struct Item {
        Expression args;
        double result;
        calc::ErrorType errCode;
    };

    QueueResults();

    QMutex m_mutex;         ///< \brief мьютекс, разграничивающий доступ к очереди
    QList<ExpressionResult *> m_data;   ///< \brief список результатов вычислений (история результатов)
    QList<Item> m_queue;    ///< \brief очередь результатов вычислений

    static void appendData(QQmlListProperty<ExpressionResult> *list, ExpressionResult *value);

    static int countData(QQmlListProperty<ExpressionResult> *list);

    static ExpressionResult *atData(QQmlListProperty<ExpressionResult> *list, int index);

    static void clearData(QQmlListProperty<ExpressionResult> *list);

signals:

    /// \brief Уведомление о наличии результата вычислений в очереди
    void notify();
};

#endif // QUEUERESULTS_H
