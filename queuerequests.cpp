#include "queuerequests.h"
#include <QMutexLocker>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(queueRequests, "queueRequests", QtWarningMsg)

QueueRequests::QueueRequests()
{
    m_wantExit = false;
    m_waitTime_sec = 1;
}

QueueRequests::~QueueRequests() {

    exitAll();
}

void QueueRequests::exitAll() {

    m_mutex.lock();
    m_wantExit = true;
    m_availableItem.wakeAll();
    m_mutex.unlock();
}

void QueueRequests::push(const Expression &item) {

    {
        QMutexLocker locker(&m_mutex);

        qCDebug(queueRequests) << QString("push: %1").arg(item.textLine());

        m_stack.push(item);
        m_availableItem.wakeOne();
    }

    emit sizeChanged();
}

void QueueRequests::setWaitTime_sec(unsigned long val) {

    QMutexLocker locker(&m_mutex);

    m_waitTime_sec = val;
    qCDebug(queueRequests) << QString("time: %1").arg(m_waitTime_sec);
}

int QueueRequests::size() {

    QMutexLocker locker(&m_mutex);

    return m_stack.size();
}

bool QueueRequests::dequeue__(Expression &item, unsigned long &time_sec) {

    {
        QMutexLocker locker(&m_mutex);
        if(m_wantExit)
            return false;

        if(m_stack.empty()) {
            m_availableItem.wait(&m_mutex);
        }

        if(m_wantExit)
            return false;

        item = m_stack.top();
        qCDebug(queueRequests) << QString("pop: %1").arg(item.textLine());
        m_stack.pop();
        time_sec = m_waitTime_sec;
    }

    emit sizeChanged();
    return true;
}

