#ifndef TASKTHREAD_H
#define TASKTHREAD_H

#include <QThread>
#include "queuerequests.h"

class TaskThread : public QThread {
public:

    TaskThread(QueueRequests *queue);

    ~TaskThread();

protected:

    void run();

private:

    QueueRequests *m_queue;

    void doTask(const Expression &args, unsigned long time_sec);
};
#endif // TASKTHREAD_H
