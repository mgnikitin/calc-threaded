TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    queuerequests.cpp \
    queueresults.cpp \
    keyprocessor.cpp \
    config.cpp \
    expression.cpp \
    taskthread.cpp

RESOURCES += qml.qrc

HEADERS += \
    queuerequests.h \
    queueresults.h \
    keyprocessor.h \
    include/calc.h \
    config.h \
    expression.h \
    taskthread.h

unix:!macx: LIBS += -L$$PWD/lib/ -lcalc

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include
