import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Window {
    visible: true

    width: Config.width
    height: Config.height
    x: Config.x
    y: Config.y

    onClosing: {
        Config.width = width
        Config.height = height
        Config.x = x
        Config.y = y
        Config.write()
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 3

        Row {
            Layout.leftMargin: 15
            Text {
                text: "queued " + QueueRequests.size
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true

            radius: 5

            ColumnLayout {
                anchors.fill: parent
                anchors.leftMargin: 15
                anchors.rightMargin: 15

                ListView {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    verticalLayoutDirection: ListView.BottomToTop
                    clip: true

                    model: QueueResults.results
                    delegate: Text {
                        width: ListView.view.width
                        text: model.modelData.text
                        color: model.modelData.isError ? "red" : (model.modelData.isResult ? "blue" : "green")
                    }

                }

                Text {
                    font.pointSize: 14
                    text: KeyProcessor.textLine
                }
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            Layout.margins: 10
            spacing: 10

            Keyboard {}

            Column {
                Operators {}
                SpinBox {
                    width: 100
                    height: 70
                    value: KeyProcessor.time

                    onValueChanged: {
                        KeyProcessor.time = value
                    }
                }
            }
        }
    }
}
